﻿using UnityEngine;
using System.Collections;

public class PongPaddle : MonoBehaviour {

	private bool moveLeft = true;
	private bool moveRight = true;
	GameObject paddlePlayer;
	GameObject pongBall;

	// Use this for initialization
	void Start () {
		paddlePlayer = GameObject.Find ("Paddle Player");
		pongBall = GameObject.Find ("Ball");
	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetKey(KeyCode.Escape)){
			Application.Quit();
		}

		if(paddlePlayer.transform.position.x > 0.37f){
			moveRight = false;
		}
		if(paddlePlayer.transform.position.x < -0.37f){
			moveLeft = false;
		}
		if(paddlePlayer.transform.position.x < 0.37f){
			moveRight = true;
		}
		if(paddlePlayer.transform.position.x > -0.37f){
			moveLeft = true;
		}

		if ((moveLeft == true) && (Input.GetKey(KeyCode.LeftArrow)))
		{
			Vector3 position = this.transform.position;
			position.x = position.x - 0.01f;
			this.transform.position = position;
		}

		if ((moveRight == true) && (Input.GetKey(KeyCode.RightArrow)))
		{
			Vector3 position = this.transform.position;
			position.x = position.x + 0.01f;
			this.transform.position = position;
		}



	
	}


}
