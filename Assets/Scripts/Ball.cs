﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

	GameObject pongBall, 
		p1,p2,p3,p4,p5, 
		c1,c2,c3,c4,c5;
	int rand, pScore, cScore;
	public Sprite greenDot;
	public Sprite redDot;
	private SpriteRenderer sRender;
	public AudioClip impact;
	public AudioClip score;
	Vector3 center = new Vector3 (0,0,0);

//RUNS ONCE ON STARTUP
	void Start () {

		pScore = 0;
		cScore = 0;
//SETS THE RANDOM START VALUE
		rand = Mathf.RoundToInt(Random.value);
//FINDS THE BALL OBJECT AND SETS IT TO pongBall
		pongBall = GameObject.Find ("Ball");
//CREATES THE SCORE OBJECTS, SETS THERE POSITIONS, ASSIGNS SPRITE COMPONENT, AND ADDS SPRITE IMAGE
		p1 = new GameObject ("pScore1");
		p1.transform.position = new Vector2 (-0.47f,-0.36f);
		p1.transform.localScale = new Vector2 (0.35f, 0.35f);
		sRender = p1.AddComponent<SpriteRenderer>();
		sRender.sprite = greenDot;

		p2 = new GameObject ("pScore2");
		p2.transform.position = new Vector2 (-0.47f,-0.32f);
		p2.transform.localScale = new Vector2 (0.35f, 0.35f);
		sRender = p2.AddComponent<SpriteRenderer>();
		sRender.sprite = greenDot;

		p3 = new GameObject ("pScore3");
		p3.transform.position = new Vector2 (-0.47f,-0.28f);
		p3.transform.localScale = new Vector2 (0.35f, 0.35f);
		sRender = p3.AddComponent<SpriteRenderer>();
		sRender.sprite = greenDot;

		p4 = new GameObject ("pScore4");
		p4.transform.position = new Vector2 (-0.47f,-0.24f);
		p4.transform.localScale = new Vector2 (0.35f, 0.35f);
		sRender = p4.AddComponent<SpriteRenderer>();
		sRender.sprite = greenDot;

		p5 = new GameObject ("pScore5");
		p5.transform.position = new Vector2 (-0.47f,-0.20f);
		p5.transform.localScale = new Vector2 (0.35f, 0.35f);
		sRender = p5.AddComponent<SpriteRenderer>();
		sRender.sprite = greenDot;

		c1 = new GameObject ("cScore1");
		c1.transform.position = new Vector2 (-0.47f,0.36f);
		c1.transform.localScale = new Vector2 (0.35f, 0.35f);
		sRender = c1.AddComponent<SpriteRenderer>();
		sRender.sprite = redDot;
		
		c2 = new GameObject ("cScore2");
		c2.transform.position = new Vector2 (-0.47f,0.32f);
		c2.transform.localScale = new Vector2 (0.35f, 0.35f);
		sRender = c2.AddComponent<SpriteRenderer>();
		sRender.sprite = redDot;
		
		c3 = new GameObject ("cScore3");
		c3.transform.position = new Vector2 (-0.47f,0.28f);
		c3.transform.localScale = new Vector2 (0.35f, 0.35f);
		sRender = c3.AddComponent<SpriteRenderer>();
		sRender.sprite = redDot;
		
		c4 = new GameObject ("cScore4");
		c4.transform.position = new Vector2 (-0.47f,0.24f);
		c4.transform.localScale = new Vector2 (0.35f, 0.35f);
		sRender = c4.AddComponent<SpriteRenderer>();
		sRender.sprite = redDot;
		
		c5 = new GameObject ("cScore5");
		c5.transform.position = new Vector2 (-0.47f,0.20f);
		c5.transform.localScale = new Vector2 (0.35f, 0.35f);
		sRender = c5.AddComponent<SpriteRenderer>();
		sRender.sprite = redDot;
	}
	
//RUNS PER FRAME
	void Update () {

		if(pScore == 0){
			p1.renderer.enabled = false;
			p2.renderer.enabled = false;
			p3.renderer.enabled = false;
			p4.renderer.enabled = false;
			p5.renderer.enabled = false;
		}

		if(cScore == 0){
			c1.renderer.enabled = false;
			c2.renderer.enabled = false;
			c3.renderer.enabled = false;
			c4.renderer.enabled = false;
			c5.renderer.enabled = false;
		}

		//if ((moveRight == true) && (Input.GetKey(KeyCode.RightArrow)))
		if((pongBall.transform.position.x == 0) && (pongBall.transform.position.y == 0)){

			if (Input.GetKeyDown (KeyCode.Space)) {

					pongBall.rigidbody.velocity = new Vector2 (0, Random.Range (-0.5f, 0.5f));

			}
		}
		if(pongBall.rigidbody.position.y > 0.5f){

			pongBall.GetComponent<AudioSource>();
			//pongBall.audio.PlayOneShot(impact, 1.0f);
			AudioSource.PlayClipAtPoint(score, center);

			pScore ++;
			pongBall.rigidbody.velocity = new Vector2(0,0);
			pongBall.rigidbody.position = new Vector2 (0,0);

			switch (pScore){
			case 1:
				p1.renderer.enabled = true;
				break;
			case 2:
				p2.renderer.enabled = true;
				break;
			case 3:
				p3.renderer.enabled = true;
				break;
			case 4:
				p4.renderer.enabled = true;
				break;
			case 5:
				p5.renderer.enabled = true;

				pScore = 0;
				cScore = 0;

				break;
			default:
				break;
			}
			Debug.Log("Player Scores");
		}
		if(pongBall.rigidbody.position.y < -0.5f){

			pongBall.GetComponent<AudioSource>();
			//pongBall.audio.PlayOneShot(impact, 1.0f);
			AudioSource.PlayClipAtPoint(score, center);

			cScore++;
			pongBall.rigidbody.velocity = new Vector2(0,0);
			pongBall.rigidbody.position = new Vector2 (0,0);

			switch (cScore){
			case 1:
				c1.renderer.enabled = true;
				break;
			case 2:
				c2.renderer.enabled = true;
				break;
			case 3:
				c3.renderer.enabled = true;
				break;
			case 4:
				c4.renderer.enabled = true;
				break;
			case 5:
				c5.renderer.enabled = true;
				cScore = 0;
				pScore = 0;
				break;
			default:
				break;
			}

			Debug.Log("Computer Scores");
		}


	}



}
