﻿using UnityEngine;
using System.Collections;

public class CheckBallColl : MonoBehaviour {
	Vector3 center = new Vector3 (0,0,0);
	Vector3 stop = new Vector3(0,5,0);
	Vector3 stopUp = new Vector3 (0,-5,0);
	Vector3 wallStop = new Vector3 (0,5,0);
	float lastY;
	public AudioClip impact;

	void OnCollisionEnter(Collision col){
		GameObject paddle = GameObject.Find ("Paddle Player");
		GameObject comp = GameObject.Find ("Paddle Comp");
		GameObject bal = GameObject.Find ("Ball");

		if(col.gameObject.name == "Paddle Player"){
			float xDiff = Mathf.Abs(5*(Mathf.Abs(bal.transform.position.x) - Mathf.Abs(paddle.transform.position.x)));
			//Debug.Log(xDiff);
			Debug.Log("Should be playing sound");

			bal.GetComponent<AudioSource>();
			bal.audio.PlayOneShot(impact, 1.0f);
			AudioSource.PlayClipAtPoint(impact, center);

			float xTemp = bal.rigidbody.velocity.x;
			float yTemp = bal.rigidbody.velocity.y;

			if (xTemp > 0) {
				bal.rigidbody.velocity = new Vector3 (xTemp + xDiff, Mathf.Abs(yTemp)+0.25f,0);
			}
			if (xTemp < 0) {
				bal.rigidbody.velocity = new Vector3 (xTemp - xDiff, Mathf.Abs(yTemp)+0.25f,0);
			}
			if(xTemp == 0){
				if(paddle.transform.position.x < bal.transform.position.x){
					bal.rigidbody.velocity = new Vector3 (xDiff, Mathf.Abs(yTemp)+0.25f,0);
				}
				if(paddle.transform.position.x > bal.transform.position.x){
					bal.rigidbody.velocity = new Vector3 (-xDiff, Mathf.Abs(yTemp)+0.25f,0);
				}
				if(paddle.transform.position.x == bal.transform.position.x){
					bal.rigidbody.velocity = new Vector3 (0, Mathf.Abs(yTemp)+0.25f,0);
				}
				//bal.rigidbody.velocity = new Vector3 (xDiff, Mathf.Abs(yTemp)+0.25f,0);
			}
				//bal.rigidbody.velocity = new Vector3 (xTemp + xDiff, Mathf.Abs(yTemp)+0.25f,0);
			//}

		}

		if(col.gameObject.name == "Paddle Comp"){
			//audio.PlayOneShot(impact, 1.0f);

			bal.GetComponent<AudioSource>();
			bal.audio.PlayOneShot(impact, 1.0f);
			AudioSource.PlayClipAtPoint(impact, center);

			float xDiff = 5*(Mathf.Abs(bal.transform.position.x) - Mathf.Abs(paddle.transform.position.x));
			//Debug.Log(xDiff);
			float xTemp = bal.rigidbody.velocity.x;
			float yTemp = bal.rigidbody.velocity.y*-1;
			//bal.rigidbody.velocity = new Vector3 (xTemp + xDiff, yTemp-0.25f, 0);
			if (xTemp >= 0) {
				bal.rigidbody.velocity = new Vector3 (xTemp + xDiff, yTemp-0.25f,0);
			}
			if (xTemp <= 0) {
				bal.rigidbody.velocity = new Vector3 (xTemp - xDiff, yTemp-0.25f,0);
			}
		}

		if(col.gameObject.name == "WallLeft"){
			bal.GetComponent<AudioSource>();
			bal.audio.PlayOneShot(impact, 1.0f);
			AudioSource.PlayClipAtPoint(impact, center);

			lastY = bal.rigidbody.velocity.y;
			float xTemp = bal.rigidbody.velocity.x;

			if(bal.rigidbody.velocity.y < 0){
				Vector3 tempV = new Vector3(Mathf.Abs(xTemp), bal.rigidbody.velocity.y - 0.1f, 0);
				bal.rigidbody.velocity = tempV;
			}
			if(bal.rigidbody.velocity.y > 0){
				Vector3 tempV = new Vector3(Mathf.Abs(xTemp), bal.rigidbody.velocity.y + 0.1f, 0);
				bal.rigidbody.velocity = tempV;
			}

			//Vector3 tempV = new Vector3(Mathf.Abs(xTemp), bal.rigidbody.velocity.y, 0);
			//bal.rigidbody.velocity = tempV;
			/*if(bal.rigidbody.velocity.y == 0){
				bal.rigidbody.velocity = new Vector3(Mathf.Abs(xTemp),lastY, 0);
			}*/
			//Debug.Log(bal.rigidbody.velocity);

		}

		if(col.gameObject.name == "WallRight"){
			bal.GetComponent<AudioSource>();
			bal.audio.PlayOneShot(impact, 1.0f);
			AudioSource.PlayClipAtPoint(impact, center);

			lastY = bal.rigidbody.velocity.y;
			float xTemp = bal.rigidbody.velocity.x;

			if(bal.rigidbody.velocity.y < 0){
				Vector3 tempV = new Vector3(xTemp*-1, bal.rigidbody.velocity.y - 0.1f, 0);
				bal.rigidbody.velocity = tempV;
			}
			if(bal.rigidbody.velocity.y > 0){
				Vector3 tempV = new Vector3(xTemp*-1, bal.rigidbody.velocity.y + 0.1f, 0);
				bal.rigidbody.velocity = tempV;
			}

			bal.rigidbody.velocity = new Vector3(xTemp*-1, bal.rigidbody.velocity.y, 0);

		}
	}

}
